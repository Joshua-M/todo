Rails.application.routes.draw do
  resources :books
  resources :tasks do
    member do
      put :complete
    end
  end
  resources :user
  root 'pages#home'

  get 'signup'    => 'pages#signup'
  post 'signin'   => 'sessions#signin'
  delete 'signout'  => 'sessions#signout'
end
