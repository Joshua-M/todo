module TasksHelper

  def set_user_id(task_params)
    task_params[:user_id] = session[:current_user_id]
    task_params
  end
end
