class TasksController < ApplicationController
  before_action :logged_in?, :set_task, only: %i[ show edit update destroy complete]
  
  def index
    @pagy, @tasks = pagy(User.find(session[:current_user_id]).task, items: 10)
    @user = current_user
  end

  def show
  end

  def new
    @task = Task.new
  end

  def edit
  end

  def create
    @task = Task.new(set_user_id(task_params))
    respond_to do |format|
      if @task.save
        format.html { redirect_to tasks_url, notice: "Task was successfully created." }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @task.update(task_params)
        format.html { redirect_to tasks_url, notice: "Task was successfully updated." }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @task.destroy
    respond_to do |format|
      format.html { redirect_to tasks_url, notice: "Task was successfully destroyed." }
    end
  end

  def complete
    respond_to do |format|
      if @task.update_attribute(:status, true)
        format.html { redirect_to tasks_url, notice: "Task was successfully updated." }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end

  private

    def set_task
      @task = Task.find(params[:id])
      @user = current_user
    end

    def task_params
      params.require(:task).permit(:description)
    end
end
