class UserController < ApplicationController
  before_action :logged_in?, only: %i[ show update edit ]
  before_action :set_user, only: %i[ edit update]

  def index
    render json: User.all
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to root_path, flash: {notice: "Account created for #{params[:email]}"}
    else
      redirect_to signup_path, flash: {error: "Error creating an account"}
    end
  end

  def show
  end
  
  def update
    respond_to do |format|
      if @user.update(user_update_params)
        format.html { redirect_to edit_user_path(@user), flash: {notice: "Update successful!"} }
      else
        format.html { redirect_to edit_user_path(@user), flash: {notice: "Update failed!"} }
      end
    end
  end
  
  def edit
  end
  
  private

  def user_params
    params.permit(:name, :email, :password, :password_confirmation)
  end

  def user_update_params
    params.require(:user).permit(:name, :email, :image)
  end

  def set_user
    @user = User.find_by(id: params[:id])
  end
end
