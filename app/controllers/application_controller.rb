class ApplicationController < ActionController::Base
  include Pagy::Backend
  include ApplicationHelper

  def logged_in?
    if !session[:current_user_id]
      redirect_to root_path and return
    end
  end
end
