class PagesController < ApplicationController
  before_action :logged_in?, only: %i[ home signup ]
  layout 'home'

  def home
  end

  def signup
  end

  private

  def logged_in?
    if session[:current_user_id]
      redirect_to tasks_path
    end
  end
end
