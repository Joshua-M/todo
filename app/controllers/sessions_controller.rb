class SessionsController < ApplicationController

  def signin
    @user = User.where(email: params[:email]).first
    if @user&.authenticate(params[:password])
      redirect_to root_path, flash: {error: 'Email or Password do not match!'} and return
    end
    session[:current_user_id] = @user.id
    redirect_to tasks_path
  end

  def signout
    reset_session
    redirect_to root_path
  end
end
